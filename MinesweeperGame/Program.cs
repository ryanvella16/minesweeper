﻿using System;

namespace MinesweeperGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int lines = 0, columns = 0, fields = 0, counter = 0;

            while(true) { 
            Console.WriteLine("Enter lines");
            lines = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter columns");
            columns = Convert.ToInt32(Console.ReadLine());

                if(0 == columns && 0 == lines)
                {
                    break;
                }
                fields++;
                char[,] input = new char[lines, columns];
                String[,] output = new string[lines, columns];

                char[] c = null;
                for (int i = 0; i < lines; i++)
                {
                    c = Console.ReadLine().ToCharArray();
                    for (int j = 0; j < c.Length; j++)
                    {
                        input[i, j] = c[j];
                    }
                    for (int j = c.Length; j < columns; j++)
                    {
                        input[i, j] = '0';
                    }
                }

                for (int i = 0; i< lines; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {
                        counter = 0;
                        if ((j+1) < columns)
                        {
                            counter += input[i, j + 1].Equals('*') ? 1 : 0;
                            if ((i-1) >= 0)
                            {
                                counter += input[i - 1, j + 1].Equals('*') ? 1 : 0;
                            }
                            if ((i+1) < lines)
                            {
                                counter += input[i + 1, j + 1].Equals('*') ? 1 : 0;
                            }
                        }
                        if ((j - 1) >= 0)
                        {
                            counter += input[i, j - 1].Equals('*') ? 1 : 0;
                            if ((i - 1) >= 0)
                            {
                                counter += input[i - 1, j - 1].Equals('*') ? 1 : 0;
                            }
                            if ((i + 1) < lines)
                            {
                                counter += input[i + 1, j - 1].Equals('*') ? 1 : 0;
                            }

                        }
                        if((i-1) >= 0)
                        {
                            counter += input[i - 1, j].Equals('*') ? 1 : 0;
                        }
                        if((i+1) < lines)
                        {
                            counter += input[i + 1, j].Equals('*') ? 1 : 0;
                        }

                        if('*' != input [i,j])
                        {

                            output[i, j] = counter.ToString();
                        } else
                        {
                            output[i, j] = "*";
                        }
                    }
                }
                print(fields, lines, columns, output);
            }
        }

        static void print (int field, int lines, int columns, String[,] output)
        {
            Console.WriteLine("Field #" + field +":");
            for (int i = 0; i < lines; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write(output[i, j]);
                }
                Console.WriteLine();
            
            }
            Console.WriteLine();
        }

    }
}
